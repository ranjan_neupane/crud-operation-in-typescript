import http from '../http-common';

class TutorialDataService {
    public getAll() {
        return http.get('/tutorials');
    }
    public get(id: string) {
        return http.get('/tutorials/${id}');
    }
    public create(data: any) {
        return http.post('/tutorials', data);
    }

    public update(id: string, data: any) {
        return http.put(`/tutorials/${id}`, data);
    }

    public delete(id: string) {
        return http.delete(`/tutorials/${id}`);
    }

    public deleteAll() {
        return http.delete(`/tutorials`);
    }

    public findByTitle(title: string) {
        return http.get(`/tutorials?title=${title}`);
    }
}

export default new TutorialDataService();
